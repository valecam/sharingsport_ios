//
//  Config.swift
//  sharingsport
//
//  Created by Valentin Le Cam on 07/02/2017.
//  Copyright © 2017 Valentin Le Cam. All rights reserved.
//

import Foundation

//Url pour les webservices 
let URL_WEBSERVICES = "https://sharingsport.fr/Symfony/web/app_dev.php"

//Clé d'identification pour les web services
let SECURITY_KEY = "6b6862756a62756935366d6f69626e696f3839363735756f62756f62"

//Content Type
let CONTENT_TYPE = "application/x-www-form-urlencoded"

//Header for webservices
let HEADERS = [
    "SecurityKey": SECURITY_KEY,
    "Content-Type": CONTENT_TYPE
]
