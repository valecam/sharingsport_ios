//
//  AuthentificationViewController.swift
//  sharingsport
//
//  Created by Valentin Le Cam on 09/02/2017.
//  Copyright © 2017 Valentin Le Cam. All rights reserved.
//

import UIKit
import Security
import Alamofire
import FBSDKLoginKit

class AuthentificationViewController: UIViewController, UITextFieldDelegate, FBSDKLoginButtonDelegate {



    @IBOutlet weak var textEmail: UITextField!
    @IBOutlet weak var textPassword: UITextField!
    @IBOutlet weak var buttonAuthentification: UIButton!
    
    @IBOutlet weak var viewAuthentification: UIView!
    @IBOutlet weak var buttonAuthentificationFacebook: FBSDKLoginButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.backIndicatorImage  = UIImage(named: "backButton")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "backButton")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        viewAuthentification.layer.cornerRadius = 2
        viewAuthentification.layer.shadowColor = UIColor(red:0/255.0, green:0/255.0, blue: 0/255.0, alpha: 1.0).cgColor
        viewAuthentification.layer.shadowOffset = CGSize(width: 0, height: 1.75)
        viewAuthentification.layer.shadowRadius = 1.7
        viewAuthentification.layer.shadowOpacity = 0.45

        
        buttonAuthentificationFacebook.frame = CGRect(x: 0, y: 0, width: 0, height: 42)
    
        //Delegate textfield
        self.textEmail.delegate = self
        self.textPassword.delegate = self
    
        //Delegate button facebook
        buttonAuthentificationFacebook.readPermissions = ["public_profile", "email", "user_friends"]
        FBSDKProfile.enableUpdates(onAccessTokenChange: true)
        buttonAuthentificationFacebook.delegate = self
        
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DismissKeyboard))
        view.addGestureRecognizer(tap)
        
  
        
        
        textPassword.layer.borderWidth = 1.0
        textPassword.layer.borderColor = UIColor(hex: "#DDDDDD").cgColor
        textPassword.layer.cornerRadius = 2
        
        //Custom button
        buttonAuthentification.layer.cornerRadius = 2
        


        // Do any additional setup after loading the view.
    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print ("Did log out of facebook")
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {

            if error != nil {
                print (error)
                return
            }
            print ("Succes")
        FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "id, name, email"]).start {
                (connection, result, err) in
            if err != nil {
                print ("Failed to start graph request:", err)
                return
            }
            print (result)
        }
        
    
    }
    
    @IBAction func clickConnectionButton(_ sender: Any) {

        
        let frameActivity = CGRect(x: UIScreen.main.bounds.width/2 - 20, y: 9, width: 30, height: 30)
        let activity = UIActivityIndicatorView(frame: frameActivity)
        activity.hidesWhenStopped = true
        (sender as AnyObject).addSubview(activity)
        activity.startAnimating()
        self.buttonAuthentification.titleLabel?.removeFromSuperview()
        
        let paramsList:[String: String] = [ "email": textEmail.text!, "password": Tools.AESEncryption(self.textPassword.text!).hexadecimalStringUsingEncoding(String.Encoding.utf8)!, "deviceToken":deviceToken ]
        
        Alamofire.request(WS_GET_AUTHENTIFICATION, method: .post, parameters: paramsList, encoding: URLEncoding.default, headers: HEADERS).responseJSON { response in

        switch response.result {
        case .success:
            let json = Tools.parseJSON(response.data!)
            
            if let success:Int = json["success"] as? Int {
                
                if( success == 1){
                    if let result:NSDictionary = json["result"]! as? NSDictionary {
                        
                        let defaults = UserDefaults.standard
                        
                       
                        
                        //User Title
                        defaults.setValue(result["display_name"] as! String, forKey: "userTitle")
                        
                        //User Id
                        defaults.setValue(result["id"] as! Int, forKey: "userId")
                        
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let startViewController: UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "MainTabBar") as! UITabBarController
                        startViewController.selectedIndex = 2
                        appDelegate.window?.rootViewController = startViewController
                        appDelegate.window?.makeKeyAndVisible()
                    }
                }
                else{
                    let error:String = json["error"]! as! String
                    
                    /*if(error == "email_false"){
                        self.errorLabel.text = "Aucun compte lié à cette adresse email"
                    }
                    if(error == "password_false"){
                        self.errorLabel.text = "Le mot de passe lié à cette adresse email est faux"
                    }*/
                }
            }
            
            self.removeActivityIndicator()
        case .failure(let error):
            print(error)
            self.removeActivityIndicator()

            //No Connection view
        }
    }
    }


func removeActivityIndicator() {
    let activity = self.buttonAuthentification.subviews.last
    activity?.removeFromSuperview()
    self.buttonAuthentification.addSubview(self.buttonAuthentification.titleLabel!)
}

func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
    if(self.textPassword.isEditing) {
        self.view.endEditing(true)
    } else {
        if let nextResponder: UIResponder? = self.textPassword {
            nextResponder?.becomeFirstResponder()
        }
        else {
            // Not found, so remove keyboard.
            self.textPassword.resignFirstResponder()
        }
    }
    return false
    }


    
    //Calls this function when the tap is recognized.
    func DismissKeyboard(){
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

}
