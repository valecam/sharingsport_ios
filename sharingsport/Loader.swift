//
//  Loader.swift
//  sharingsport
//
//  Created by Valentin Le Cam on 04/02/2017.
//  Copyright © 2017 Valentin Le Cam. All rights reserved.
//


import UIKit

class Loader: UIView {
    
    var pageEmpty = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addCustomView()
    }
    
    init(pageEmpty:Bool) {
        super.init(frame: CGRect.zero)
        self.pageEmpty = pageEmpty
        self.addCustomView()
    }
    
    init() {
        super.init(frame: CGRect.zero)
        self.addCustomView()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addCustomView() {
        self.frame = UIScreen.main.bounds
        
        if(self.pageEmpty) {
            let empty = UIView(frame: self.frame)
            empty.backgroundColor = UIColor(hex: "#EEEEEE")
            self.addSubview(empty)
        }
        
        let imageLoader = UIImageView(image: UIImage(named: ""))
        imageLoader.frame.size = CGSize(width: 60, height: 60)
        imageLoader.center = self.center
        self.rotateImage(imageLoader)
        
        self.addSubview(imageLoader)
    }
    
    func rotateImage(_ imageToRotate:UIImageView) {
        // create a spin animation
        let spinAnimation = CABasicAnimation()
        // starts from 0
        spinAnimation.fromValue = 0
        // goes to 360 ( 2 * π )
        spinAnimation.toValue = M_PI*2
        // define how long it will take to complete a 360
        spinAnimation.duration = 1
        // make it spin infinitely
        spinAnimation.repeatCount = Float.infinity
        // do not remove when completed
        spinAnimation.isRemovedOnCompletion = false
        // specify the fill mode
        spinAnimation.fillMode = kCAFillModeForwards
        // and the animation acceleration
        spinAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        // add the animation to the button layer
        imageToRotate.layer.add(spinAnimation, forKey: "transform.rotation.z")
    }
    
}
