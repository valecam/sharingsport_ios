//
//  AnnouncementUserTimelineViewController.swift
//  sharingsport
//
//  Created by Valentin Le Cam on 05/02/2017.
//  Copyright © 2017 Valentin Le Cam. All rights reserved.
//

import UIKit
import Alamofire

class AnnouncementUserTimelineViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var tableView: UITableView!
    
    /* Parameters */
    var paramsList = [String:String]()
    
    /* List announcement */
    var listAnnouncements = [Announcement]()
    
    /*Pull to refresh */
    var refreshControl = UIRefreshControl()
    
    /* Loader */
    var loader = Loader()

    override func viewDidLoad() {
        super.viewDidLoad()

        //First Load
        self.loader = Loader(pageEmpty: true)
        self.view.addSubview(self.loader)
        
     
        //Important : Permet d'éviter les bugs d'affichage des scrollView
        self.automaticallyAdjustsScrollViewInsets = false
        
        //Set color on navigation bar
        self.navigationController?.navigationBar.barTintColor = UIColor(hex: "#25a45b")

        
        //View cell
        let nib = UINib(nibName: "viewAnnouncementMaterialTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "announcementCell")
        
        self.getAnnouncement()
        // Do any additional setup after loading the view.
    }

    func segmentValueChanged(_ sender: AnyObject?){
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    /* Height of rows in the tableView */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listAnnouncements.count
    }
    
    /* Number of sections in tableView */
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "announcementCell", for: indexPath) as!  AnnouncementMaterialTableViewCell
        
        let whiteRoundedView : UIView = UIView(frame: CGRect(x: 0, y: 8, width: self.view.frame.size.width+30 , height: 140))
        
        whiteRoundedView.layer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1.0, 1.0, 1.0, 0.9])
        whiteRoundedView.layer.cornerRadius = 4
        whiteRoundedView.layer.shadowOffset = CGSize(width: 0, height: 1)
        whiteRoundedView.layer.shadowOpacity = 0.1
        whiteRoundedView.layer.borderWidth = 0.5
        whiteRoundedView.layer.borderColor = UIColor(hex: "#DDDDDD").cgColor
        cell.contentView.addSubview(whiteRoundedView)
        cell.contentView.sendSubview(toBack: whiteRoundedView)
        
        cell.layer.shadowColor = UIColor.lightGray.cgColor
        
        let announcement = self.listAnnouncements[indexPath.row]
        
        cell.lbTitleAnnouncement.text = announcement.announcementTitle
        cell.lbDescriptionAnnouncement.text = announcement.announcementDescription
        cell.lbPriceAnnouncement.text = "\(announcement.announcementPrice) € "
        cell.lbDateBegin.text = Tools.parseDate(announcement.dateBegin)
        
        return cell
        
    }
    
    
    func getAnnouncement() {
        Alamofire.request(WS_GET_ANNOUNCEMENTS_MATERIALS, method: .post, parameters: paramsList, encoding: URLEncoding.default, headers: HEADERS).responseJSON { response in
            switch response.result {
            case .success:
                let json = Tools.parseJSON(response.data!)
                print(json)
                if let success:Int = json["success"] as? Int {
                    if(success == 1){
                        
                        //Clear list
                        self.listAnnouncements = []
                        
                        if let result = json["result"]! as? NSMutableArray {
                            for announcement in result{
                                self.listAnnouncements.append(Announcement(object: announcement as! NSDictionary))
                            }
                        }
                        
                        
                        self.tableView.reloadData()
                        
                        //Hide progress
                        self.loader.isHidden = true
                        
                        //End of refresh
                        self.refreshControl.endRefreshing()
        
                        
                        }
                    }
                
                
                case .failure(let error):
                print(error)
            
                //Hide progress
                self.loader.isHidden = true
                
                //End of refresh
                self.refreshControl.endRefreshing()
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        tableView.deselectRow(at: indexPath, animated: true)

        let detailsAnnouncementUser = self.storyboard?.instantiateViewController(withIdentifier: "DetailsAnnouncementUserViewController") as? DetailsAnnouncementUserViewController
        detailsAnnouncementUser?.announcementId = "\(self.listAnnouncements[indexPath.row].announcementId)"
        self.navigationController?.pushViewController(detailsAnnouncementUser!, animated: true)
        
        
    }


}
