//
//  AnnouncementTimelineViewController.swift
//  sharingsport
//
//  Created by Valentin Le Cam on 29/01/2017.
//  Copyright © 2017 Valentin Le Cam. All rights reserved.
//

import UIKit
import Alamofire

class AnnouncementTimelineViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var segmentedControl: SegmentedControl!
    
    
    /* Parameters */
    var paramsList = [String:String]()

    /* List announcement */    
    var listAnnouncements = [Announcement]()

    override func viewDidLoad() {
        super.viewDidLoad()

        //Important : Permet d'éviter les bugs d'affichage des scrollView
        self.automaticallyAdjustsScrollViewInsets = false
        
        //Shadown border bottom on segmentedControl
        self.segmentedControl.layer.shadowColor  = UIColor(hex: "#DDDDDD").cgColor
        self.segmentedControl.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.segmentedControl.layer.shadowOpacity = 0.4
        self.segmentedControl.layer.shadowRadius = 1
     
        
        //View cell
        let nib = UINib(nibName: "viewAnnouncementMaterialTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "announcementCell")
        
        //Segmented control
        self.segmentedControl.selectedIndex = 1
        self.segmentedControl.addTarget(self, action: #selector(self.segmentValueChanged(_:)), for: .valueChanged)
        
        
        //Delete border on navigationBar
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        // Do any additional setup after loading the view.
    }
    

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /* Height of rows in the tableView */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 280
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    /* Number of sections in tableView */
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "announcementCell", for: indexPath) as!  AnnouncementMaterialTableViewCell

        
        let whiteRoundedView : UIView = UIView(frame: CGRect(x: 10, y: 8, width: self.view.frame.size.width - 15, height: 140))
        
        whiteRoundedView.layer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1.0, 1.0, 1.0, 0.9])
        whiteRoundedView.layer.cornerRadius = 2.0
        whiteRoundedView.layer.shadowOffset = CGSize(width: 0, height: 1)
        whiteRoundedView.layer.shadowOpacity = 0.1
        whiteRoundedView.layer.borderWidth = 0.5
        whiteRoundedView.layer.borderColor = UIColor(hex: "#DDDDDD").cgColor
        cell.contentView.addSubview(whiteRoundedView)
        cell.contentView.sendSubview(toBack: whiteRoundedView)
        
        cell.layer.shadowColor = UIColor.lightGray.cgColor
        
        return cell

    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func segmentValueChanged(_ sender: AnyObject?){
        self.tableView.reloadData()
    }


}
