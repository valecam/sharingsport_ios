//
//  WebServices.swift
//  sharingsport
//
//  Created by Valentin Le Cam on 07/02/2017.
//  Copyright © 2017 Valentin Le Cam. All rights reserved.
//

import Foundation



/** GET */
let WS_GET_ANNOUNCEMENTS_MATERIALS = "\(URL_WEBSERVICES)/getAnnouncementsMaterials/"
let WS_GET_ANNOUNCEMENTS_PARTNERS = "\(URL_WEBSERVICES)/getAnnouncementsPartners/"
let WS_GET_DETAILS_ANNOUNCEMENT = "\(URL_WEBSERVICES)/getDetailsAnnouncement/"
let WS_GET_AUTHENTIFICATION = "\(URL_WEBSERVICES)/getAuthentification/"

/** SEND */


