//
//  NSData.swift
//  sharingsport
//
//  Created by Valentin Le Cam on 09/02/2017.
//  Copyright © 2017 Valentin Le Cam. All rights reserved.
//

import Foundation

extension Data {
    
    /// Create hexadecimal string representation of NSData object.
    ///
    /// :returns: String representation of this NSData object.
    
    public func hexadecimalString(inGroupsOf characterCount: Int = 0) -> String {
        var string = ""
        for (index, value) in self.enumerated() {
            if characterCount != 0 && index > 0 && index % characterCount == 0 {
                string += " "
            }
            string += (value < 16 ? "0" : "") + String(value, radix: 16)
        }
        return string
    }
}
