
//
//  FilterSearchViewController.swift
//  sharingsport
//
//  Created by Valentin Le Cam on 20/02/2017.
//  Copyright © 2017 Valentin Le Cam. All rights reserved.
//

import UIKit

class FilterSearchViewController: UIViewController {

    @IBOutlet weak var backButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickBackButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)

    }
    
}
