//
//  AnnouncementMaterialTableViewCell.swift
//  sharingsport
//
//  Created by Valentin Le Cam on 29/01/2017.
//  Copyright © 2017 Valentin Le Cam. All rights reserved.
//

import UIKit
import Alamofire


class AnnouncementMaterialTableViewCell: UITableViewCell {

    @IBOutlet weak var ivAnnouncement: UIImageView!
    @IBOutlet weak var lbTitleAnnouncement: UILabel!
    @IBOutlet weak var lbPriceAnnouncement: UILabel!
    @IBOutlet weak var lbDescriptionAnnouncement: UITextView!
    @IBOutlet weak var lbDateBegin: UILabel!
    @IBOutlet weak var lbDateEnd: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        

        
        // Configure the view for the selected state
    }

}
