//
//  FavorisTimelineViewController.swift
//  sharingsport
//
//  Created by Valentin Le Cam on 06/02/2017.
//  Copyright © 2017 Valentin Le Cam. All rights reserved.
//

import UIKit

class FavorisTimelineViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        //Delete border on navigationBar
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
