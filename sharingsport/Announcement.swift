//
//  Announcement.swift
//  sharingsport
//
//  Created by Valentin Le Cam on 30/01/2017.
//  Copyright © 2017 Valentin Le Cam. All rights reserved.
//

import Foundation

    open class Announcement {
        
        var announcementId:Int = 0
        var announcementTitle:String = ""
        var announcementPrice:Int = 0
        var announcementDescription:String = ""
        var dateBegin:String = ""
        var dateEnd:String = ""
        
        
        init(announcementId:Int, announcementTitle:String, announcementPrice:Int, announcementDescription:String, dateBegin:String, dateEnd:String) {
            self.announcementId = announcementId
            self.announcementTitle = announcementTitle
            self.announcementPrice = announcementPrice
            self.announcementDescription = announcementDescription
            self.dateBegin = dateBegin
            self.dateEnd = dateEnd
        }
        
        init(object:NSDictionary) {
            self.announcementId = object["announcement_id"] as! Int
            self.announcementTitle = object["announcement_title"] as! String
            self.announcementPrice = object["announcement_price"] as! Int
            self.announcementDescription = object["announcement_description"] as! String
            self.dateBegin = object["announcement_date"] as! String
            self.dateEnd =  object["dateEnd"] as! String
        }

    }
