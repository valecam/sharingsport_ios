//
//  RegistrationViewController.swift
//  sharingsport
//
//  Created by Valentin Le Cam on 04/02/2017.
//  Copyright © 2017 Valentin Le Cam. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textFirstName: UITextField!
    @IBOutlet weak var textName: UITextField!

    

    
    @IBOutlet weak var buttonInscription: UIButton!
    

    

    var paramsList = [ "": ""]

    override func viewDidLoad() {
        super.viewDidLoad()

        //Delete border on navigationBar
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        //Keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)

        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

    
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }


}
