//
//  Tools.swift
//  sharingsport
//
//  Created by Valentin Le Cam on 07/02/2017.
//  Copyright © 2017 Valentin Le Cam. All rights reserved.
//

import Foundation
import AVFoundation
import SystemConfiguration
import UIKit
import Alamofire
import RNCryptor

class Tools {
    
    
    init(){
    }
    
    
    /**
     Modification de l'initial View Controller
     */
    /*
    static func customInitialViewController(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let defaults = UserDefaults.standard
        print(defaults.bool(forKey: "isConnected"))
        if(!defaults.bool(forKey: "isConnected")){
            Tools.setAlreadyLaunch(true)
            let startViewController: UINavigationController = mainStoryboard.instantiateViewController(withIdentifier: "LauchingViewController") as! UINavigationController
            appDelegate.window?.rootViewController = startViewController
            UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
        }
        else{
            let startViewController: UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "MainTabBar") as! UITabBarController
            appDelegate.window?.rootViewController = startViewController
            UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
        }
        //        let startViewController: UINavigationController = mainStoryboard.instantiateViewControllerWithIdentifier("InterestsCreationViewController") as! UINavigationController
        //        appDelegate.window?.rootViewController = startViewController
        appDelegate.window?.makeKeyAndVisible()
    }
    */
    /**
     Parse le JSON
     
     - parameter NSData: JSON
     :return: NSDictionary JSON parsé
     */
    static func parseJSON(_ inputData: Data) -> NSDictionary{
        var boardsDictionary = NSDictionary()
        do {
            boardsDictionary = try JSONSerialization.jsonObject(with: inputData, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
        } catch {
            //error
        }
        
        return boardsDictionary
    }
    
    static func AESEncryption( _ password: String)->String{
        let data: Data = password.data(using: String.Encoding.utf8)!
        let password = "12345678901234567890123456789012"
        let ciphertext = RNCryptor.encrypt(data: data, withPassword: password)
        let base64cryptString: String = ciphertext.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters)
        
        return base64cryptString
    }
    
    static func setPaddingTextField(_ textField:UITextField) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
        textField.leftView = paddingView
        textField.leftViewMode = UITextFieldViewMode.always
    }
    
    static func setError(_ textField:UITextField, text:String, error:UILabel) {
        textField.layer.borderColor = UIColor.red.cgColor
        textField.layer.borderWidth = 1
        textField.rightViewMode = UITextFieldViewMode.always
        error.text = text
        error.isHidden = false
        
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: textField.center.x - 10, y: textField.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: textField.center.x + 10, y: textField.center.y))
        textField.layer.add(animation, forKey: "position")
    }
    
    static func setNoError(_ textField:UITextField, error:UILabel) {
        textField.layer.borderWidth = 0
        error.isHidden = true
    }
    
    static func parseDate(_ date:String)->String {
        var dateParse = ""
        
        //This is what webserv send us
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        
        let dateNSDate = dateFormatter.date(from: date)
        
        //And this is what we show on the application
        dateFormatter.dateFormat = "dd MMMM yyyy"
        dateParse = "\(dateFormatter.string(from: dateNSDate!))"
        
        
        
        return dateParse
    }
    
    

    static func setAlreadyLaunch(_ bool: Bool) {
        let defaults = UserDefaults.standard
        defaults.set(true, forKey: "alreadyLaunch")
    }
    
    
    
}
