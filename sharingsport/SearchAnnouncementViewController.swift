//
//  SearchAnnouncementViewController.swift
//  sharingsport
//
//  Created by Valentin Le Cam on 07/02/2017.
//  Copyright © 2017 Valentin Le Cam. All rights reserved.
//

import UIKit
import Alamofire

class SearchAnnouncementViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var viewGlobal: UIView!

    @IBOutlet weak var viewTopSearch: UIView!
    @IBOutlet weak var buttonSearchAnnouncement: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    /* Parameters */
    var paramsList = [String:String]()
    
    /* List announcement */
    var listAnnouncements = [Announcement]()
    
    /*Pull to refresh */
    var refreshControl = UIRefreshControl()
    
    /* Loader */
    var loader = Loader()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        //Set color on navigation bar        
        self.navigationController?.navigationBar.barTintColor = UIColor.clear

        self.buttonSearchAnnouncement.layer.borderColor = UIColor.white.cgColor
        self.buttonSearchAnnouncement.layer.borderWidth = 1
        self.buttonSearchAnnouncement.layer.cornerRadius = 4
        

        
        
        viewTopSearch.layer.shadowColor = UIColor.lightGray.cgColor
        viewTopSearch.layer.shadowOpacity = 0.1
        viewTopSearch.layer.shadowOffset = CGSize(width: 0, height: 5)
        viewTopSearch.layer.shadowRadius = 10
        viewTopSearch.layer.borderWidth = 0.5
        viewTopSearch.layer.borderColor = UIColor.white.cgColor
        viewTopSearch.layer.cornerRadius = 4
        /*
        let borderMateriel = CALayer()
        borderMateriel.borderColor = UIColor(hex: "#25a45b").cgColor
        borderMateriel.borderWidth = width
        borderMateriel.frame = CGRect(x: 0, y: buttonMateriel.frame.size.height - width, width:  buttonMateriel.frame.size.width, height: buttonMateriel.frame.size.height)
        buttonMateriel.layer.addSublayer(borderMateriel)
        buttonMateriel.layer.masksToBounds = true

        let borderPartner = CALayer()
        borderPartner.borderColor = UIColor(hex: "#25a45b").cgColor
        borderPartner.borderWidth = width
        borderPartner.frame = CGRect(x: 0, y: buttonPartner.frame.size.height - width, width:  buttonPartner.frame.size.width, height: buttonPartner.frame.size.height)
        buttonPartner.layer.addSublayer(borderPartner)
        buttonPartner.layer.masksToBounds = true
        
*/

        //View cell
        let nib = UINib(nibName: "viewAnnouncementMaterialTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "announcementCell")
        
        self.getAnnouncementsMaterials()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }

    func segmentValueChanged(_ sender: AnyObject?){
        self.tableView.reloadData()

    }
    /* Height of rows in the tableView */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listAnnouncements.count
    }
    
    /* Number of sections in tableView */
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "announcementCell", for: indexPath) as!  AnnouncementMaterialTableViewCell
        
        let whiteRoundedView : UIView = UIView(frame: CGRect(x: 0, y: 8, width: self.view.frame.size.width - 20 , height: 140))

    

        whiteRoundedView.layer.cornerRadius = 6
        whiteRoundedView.layer.borderWidth = 1
        whiteRoundedView.layer.borderColor = UIColor(hex: "#DDDDDD").cgColor
        cell.contentView.addSubview(whiteRoundedView)
        cell.contentView.sendSubview(toBack: whiteRoundedView)


        let announcement = self.listAnnouncements[indexPath.row]
        
        cell.lbTitleAnnouncement.text = announcement.announcementTitle
        cell.lbDescriptionAnnouncement.text = announcement.announcementDescription
        cell.lbPriceAnnouncement.text = "\(announcement.announcementPrice) €"
        cell.lbDateBegin.text = Tools.parseDate(announcement.dateBegin)
        
        return cell
        
    }
    
    
    func getAnnouncementsMaterials() {
        Alamofire.request(WS_GET_ANNOUNCEMENTS_MATERIALS, method: .post, parameters: paramsList, encoding: URLEncoding.default, headers: HEADERS).responseJSON { response in
            switch response.result {
            case .success:
                let json = Tools.parseJSON(response.data!)
                print(json)
                if let success:Int = json["success"] as? Int {
                    if(success == 1){
                        
                        //Clear list
                        self.listAnnouncements = []
                        
                        if let result = json["result"]! as? NSMutableArray {
                            for announcement in result{
                                self.listAnnouncements.append(Announcement(object: announcement as! NSDictionary))
                            }
                        }
                        
                        
                        self.tableView.reloadData()
                        
                        //Hide progress
                        self.loader.isHidden = true
                        
                        //End of refresh
                        self.refreshControl.endRefreshing()
                        
                        
                    }
                }
                
                
            case .failure(let error):
                print(error)
                
                //Hide progress
                self.loader.isHidden = true
                
                //End of refresh
                self.refreshControl.endRefreshing()
                
            }
        }
    }
    
    
    

    
    

}
