//
//  DetailsAnnouncementUserViewController.swift
//  sharingsport
//
//  Created by Valentin Le Cam on 07/02/2017.
//  Copyright © 2017 Valentin Le Cam. All rights reserved.
//

import UIKit
import Alamofire


class DetailsAnnouncementUserViewController: UIViewController, UIScrollViewDelegate, UITextFieldDelegate, UITextViewDelegate {

    @IBOutlet weak var viewGeneral: UIScrollView!
    @IBOutlet weak var viewAnnouncementInfo: UIView!
    @IBOutlet weak var viewUserInfo: UIView!
    @IBOutlet weak var viewPriceInfo: UIView!
    
    
    /* Announcement */
    @IBOutlet weak var lbPriceTotal: UILabel!
    @IBOutlet weak var imageProfileUserAnnouncement: UIImageView!
    
    @IBOutlet weak var imageAnnouncement: UIImageView!
    @IBOutlet weak var buttonBookingAnnouncement: UIButton!

    @IBOutlet weak var lbTitleAnnouncement: UILabel!
    @IBOutlet weak var lbDescriptionAnnouncement: UITextView!
    @IBOutlet weak var lbCityAnnouncement: UILabel!

    @IBOutlet weak var textDateBeginAnnouncement: UITextField!

    /* Parameters */
    var paramsList:[String: String] = [ "": "" ]
    /* List announcement */
    var listAnnouncements = [Announcement]()
    /*Pull to refresh */
    var refreshControl = UIRefreshControl()
    /* Loader */
    var loader = Loader()
    /* Picker */
    var dateBeginPicker = UIDatePicker()
    /* Announcement Id */
    var announcementId = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Announcement Id
        self.paramsList["announcement_id"] = self.announcementId
        print ("Id annonce : \(announcementId)")
        
        //First Load
        self.loader = Loader(pageEmpty: true)
        self.view.addSubview(self.loader)
        
        
        //DateBegin picker
        self.dateBeginPicker.datePickerMode = UIDatePickerMode.date
        self.dateBeginPicker.addTarget(self, action: #selector(changeDateBegin(_:)), for: UIControlEvents.valueChanged)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: "01/01/2000")
        self.dateBeginPicker.setDate(date!, animated: false)
        self.textDateBeginAnnouncement.inputView = self.dateBeginPicker
        
        viewGeneral.layer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1.0, 1.0, 1.0, 0.9])
        
        viewAnnouncementInfo.layer.shadowColor = UIColor.lightGray.cgColor
        viewAnnouncementInfo.layer.shadowOpacity = 0.1
        viewAnnouncementInfo.layer.shadowOffset = CGSize(width: 0, height: 5)
        viewAnnouncementInfo.layer.shadowRadius = 10
        viewAnnouncementInfo.layer.borderWidth = 0.5
        viewAnnouncementInfo.layer.borderColor = UIColor.white.cgColor
        viewAnnouncementInfo.layer.cornerRadius = 4
        
        
        viewUserInfo.layer.shadowColor = UIColor.lightGray.cgColor
        viewUserInfo.layer.shadowOpacity = 0.1
        viewUserInfo.layer.shadowOffset = CGSize(width: 0, height: 5)
        viewUserInfo.layer.shadowRadius = 10
        viewUserInfo.layer.borderWidth = 0.5
        viewUserInfo.layer.borderColor = UIColor.white.cgColor
        viewUserInfo.layer.cornerRadius = 4
        
        
        viewPriceInfo.layer.shadowColor = UIColor.lightGray.cgColor
        viewPriceInfo.layer.shadowOpacity = 0.1
        viewPriceInfo.layer.shadowOffset = CGSize(width: 0, height: 5)
        viewPriceInfo.layer.shadowRadius = 10
        viewPriceInfo.layer.borderWidth = 0.5
        viewPriceInfo.layer.borderColor = UIColor.white.cgColor
        viewPriceInfo.layer.cornerRadius = 4
        
        imageAnnouncement.layer.shadowColor = UIColor.lightGray.cgColor
        imageAnnouncement.layer.shadowOpacity = 0.1
        imageAnnouncement.layer.shadowOffset = CGSize(width: 0, height: 5)
        imageAnnouncement.layer.shadowRadius = 10
        imageAnnouncement.layer.borderWidth = 0.5
        imageAnnouncement.layer.borderColor = UIColor(hex: "#DDDDDD").cgColor
        imageAnnouncement.layer.cornerRadius = 4
        
        buttonBookingAnnouncement.layer.shadowColor = UIColor.lightGray.cgColor
        buttonBookingAnnouncement.layer.shadowOpacity = 0.1
        buttonBookingAnnouncement.layer.shadowOffset = CGSize(width: 0, height: 5)
        buttonBookingAnnouncement.layer.shadowRadius = 10
        buttonBookingAnnouncement.layer.cornerRadius = 4
        
        //Important : Permet d'éviter les bugs d'affichage des scrollView
        self.automaticallyAdjustsScrollViewInsets = false
        
        
        self.imageProfileUserAnnouncement.layer.cornerRadius = self.imageProfileUserAnnouncement.frame.size.width / 2
        self.imageProfileUserAnnouncement.clipsToBounds = true;
        self.imageProfileUserAnnouncement.layer.borderWidth = 1
        self.imageProfileUserAnnouncement.layer.borderColor = UIColor(hex: "#DDDDDD").cgColor
        
        self.getData()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getData() {
        self.performSelector(inBackground: #selector(getDetailsAnnouncement), with: nil)

    }

    
    @IBAction func clickBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)

    }

    func getDetailsAnnouncement() {
        

        self.paramsList["announcement_id"] = self.announcementId
        
        Alamofire.request(WS_GET_DETAILS_ANNOUNCEMENT, method: .post, parameters: paramsList, encoding: URLEncoding.default, headers: HEADERS).responseJSON { response in
            switch response.result {
            case .success:
                let json = Tools.parseJSON(response.data!)
                print (json)
                if let success:Int = json["success"] as? Int {
                    
                    if( success == 1){
                        if let result = json["result"]! as? NSDictionary {

                            self.lbTitleAnnouncement.text = result["announcement_title"] as? String
                            self.lbDescriptionAnnouncement.text = result["announcement_description"] as? String
                            self.lbCityAnnouncement.text = result["announcement_city"] as? String
                            self.textDateBeginAnnouncement.text = result["date_begin"] as? String
                            self.lbPriceTotal.text = "\(result["announcement_price"] as! Int) €"
                        
                     
                        }
                    }
                }
                
                //Hide progress
                self.loader.isHidden = true
                
            case .failure(let error):
                print(error)
                //Hide progress
                self.loader.isHidden = true
                
                
            }
        }
    }
    
    func changeDateBegin(_ sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.short
        let strDate = dateFormatter.string(from: self.dateBeginPicker.date)
        
        self.textDateBeginAnnouncement.text = strDate
        
    }
    

}
