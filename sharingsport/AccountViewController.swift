//
//  AccountViewController.swift
//  sharingsport
//
//  Created by Valentin Le Cam on 29/01/2017.
//  Copyright © 2017 Valentin Le Cam. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController {

    @IBOutlet weak var profileImageView: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.profileImageView.layer.cornerRadius = 10.0
        self.profileImageView.layer.borderWidth = 1.0
        self.profileImageView.layer.borderColor = UIColor(hex: "#DDDDDD").cgColor
        
        
        //Set color on navigation bar
        self.navigationController?.navigationBar.barTintColor = UIColor(hex: "#25a45b")
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
