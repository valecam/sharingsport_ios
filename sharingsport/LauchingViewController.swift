//
//  LauchingViewController.swift
//  sharingsport
//
//  Created by Valentin Le Cam on 04/02/2017.
//  Copyright © 2017 Valentin Le Cam. All rights reserved.
//

import UIKit

class LauchingViewController: UIViewController {

    /* Button */
    @IBOutlet weak var buttonAuthentification: UIButton!
    @IBOutlet weak var buttonInscription: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()


        
        //Delete border on navigationBar
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated);
        super.viewWillDisappear(animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        //Authorization localisation
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.locationManager.requestAlwaysAuthorization()
    }
    
    @IBAction func clickButtonRegister(_ sender: Any) {
        let registrationViewController = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationViewController") as? RegistrationViewController
        self.navigationController?.pushViewController(registrationViewController!, animated: true)
        
    }
    @IBAction func clickButtonAuthentification(_ sender: Any) {
        let authentificationViewController = self.storyboard?.instantiateViewController(withIdentifier: "AuthentificationViewController") as? AuthentificationViewController
        self.navigationController?.pushViewController(authentificationViewController!, animated: true)
    
    }
    
    @IBOutlet weak var clickButtonRegister: UIButton!

    //Calls this function when the tap is recognized.
    func DismissKeyboard(){
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

}
