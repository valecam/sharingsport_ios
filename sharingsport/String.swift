//
//  String.swift
//  sharingsport
//
//  Created by Valentin Le Cam on 09/02/2017.
//  Copyright © 2017 Valentin Le Cam. All rights reserved.
//

import Foundation

extension String {
    
    /// Create hexadecimal string representation of String object.
    ///
    /// :param: encoding The NSStringCoding that indicates how the string should be converted to NSData before performing the hexadecimal conversion.
    ///
    /// :returns: String representation of this String object.
    
    func hexadecimalStringUsingEncoding(_ encoding: String.Encoding) -> String? {
        let data = self.data(using: String.Encoding.utf8)
        return data?.hexadecimalString()
    }
}
