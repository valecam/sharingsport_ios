//
//  Appearance.swift
//  sharingsport
//
//  Created by Valentin Le Cam on 30/01/2017.
//  Copyright © 2017 Valentin Le Cam. All rights reserved.
//

import Foundation
import UIKit


class Appearance {
    
    init(){
    }
    
    static func customApplicationColors() {

    //TabBar
    UITabBar.appearance().tintColor = UIColor(hex: "#25a45b")
    UITabBar.appearance().barTintColor = UIColor(hex: "#FFFFFF")
    
    
    UINavigationBar.appearance().backgroundColor = UIColor(hex: "#25a45b")
        UINavigationBar.appearance().tintColor = UIColor(hex:"#FFFFFF")
    

    
    }
    
}
